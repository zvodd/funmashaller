package main

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"os"
	"zvproject/funmashaller/funmarshal"
	"zvproject/funmashaller/wavlib"
)

func main() {
	convertTable := funmarshal.CopyOfConvertTable()
	// Add custom unmashal converters
	// convertTable["something"] = func([]byte, FieldTagDescriptor) (interface{}, error){}

	td := openTestWaveFile()

	riffHead := new(wavlib.RiffHeaderChunk)
	wavFmt := new(wavlib.WavFmtChunk)
	wavData := new(wavlib.WavDataChunk)
	if err := funmarshal.ReadHeader(td, riffHead, convertTable); err != nil {
		log.Fatal(err)
	}
	if err := funmarshal.ReadHeader(td, wavFmt, convertTable); err != nil {
		log.Fatal(err)
	}
	if err := funmarshal.ReadHeader(td, wavData, convertTable); err != nil {
		log.Fatal(err)
	}

	fmt.Printf("%+v\n", riffHead)
	fmt.Printf("%+v\n", wavFmt)
	fmt.Printf("%+v\n", wavData)

}

func genTestWaveHeader() io.Reader {

	td := bytes.NewBufferString("RIFF&©\x08\x00WAVE")
	fmt.Println(td.Bytes())
	return td
}

func openTestWaveFile() io.Reader {

	td, err := os.Open("testdata/power1_u8PCM.wav")
	if err != nil {
		log.Fatal("Opening file failed", err)
	}
	return td
}
