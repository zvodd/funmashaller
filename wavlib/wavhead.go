package wavlib

type RiffHeaderChunk struct {
	ChunkID   string `b:4:string`
	ChunkSize uint32 `l:4:uint`
	Format    string `b:4:string`
}
type WavFmtChunk struct {
	Subchunk1ID   string `b:4:string`
	Subchunk1Size uint32 `l:4:uint`
	AudioFormat   uint16 `l:2:uint`
	NumChannels   uint16 `l:2:uint`
	SampleRate    uint32 `l:4:uint`
	ByteRate      uint32 `l:4:uint`
	BlockAlign    uint16 `l:2:uint`
	BitsPerSample uint16 `l:2:uint`
}
type WavDataChunk struct {
	Subchunk2ID   string `b:4:string`
	Subchunk2Size uint32 `l:4:uint`
}

type WaveFile struct {
	RiffHeader RiffHeaderChunk
	WaveFormat WavFmtChunk
	WavData    WavDataChunk
}
