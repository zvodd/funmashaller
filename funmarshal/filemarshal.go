package funmarshal

// Unmashals serialized data into structs with custom tag format.

import (
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"reflect"
	"strconv"
	"strings"
)

type Endianness int

const (
	LittleEnded = iota
	BigEnded
)

// TODO Some default file location pointer for non-unmarshaled/large chunks
type FileSection struct {
	Offset uint64
	Size   uint64
}

// Unmarshalling conversion functions
// TODO Change []byte to io.X (Reader| something )
type ConvertFunc func([]byte, FieldTagDescriptor) (interface{}, error)

// Fields from struct tag, to describe Unmarshalling
type FieldTagDescriptor struct {
	Endianness Endianness
	Size       uint64
	ConvertTo  ConvertFunc
}

// Reads file-like interface and unpacks data into *Struct headerStruct,
//  utilising struct tags of format "(endieness["b"|"l"]):(size <uint64>):(convertTable key <string>)"
//  e.g. "l:8:uint" LittleEnded:8:"uint" endieness and size are passed with []bytes(len size) to convertTable["uint"]
//  convertTable maps String keys to func ptr of type ConvertFunc
// TODO Rename suitably and refactor into more readable parts.
// TODO implement FileSection Struct defining a data chuck in io.Reader (also change io.Reader to something with Seek()?)
//  * Allow using previous values to determine size of field?
//  * Refector ConvertFunc type so use Reader interface?
//  *
func ReadHeader(d io.Reader, headerStruct interface{}, convertTable map[string]ConvertFunc) error {
	// Make sure headerStruct is a pointer, so we can insert values into the struct
	hsv := reflect.ValueOf(headerStruct)
	if hsv.Kind() != reflect.Ptr || hsv.IsNil() {
		return errors.New(fmt.Sprintf("Invalid Marshal %v", reflect.TypeOf(headerStruct)))
	}

	// TODO offset to be passed into ConvertFunc? To track io position.
	var sectionOffset uint64 = 0

	for i, hFieldTag := range ReadStructDescription(headerStruct, convertTable) {
		// Read bytes of current section size
		fbuf := make([]byte, hFieldTag.Size)
		nr, err := d.Read(fbuf)
		if err != nil || uint64(nr) != hFieldTag.Size {
			return err
		}
		sectionOffset += uint64(nr)

		var newFieldValue interface{}
		if hFieldTag.ConvertTo != nil {
			newFieldValue, _ = hFieldTag.ConvertTo(fbuf, hFieldTag)
		} else {
			//TODO Error here or in ParseStructTag() on ConvertFunc field
			// or use some apporpriate null/zero value based on type?
			// No conversion neccisary?
		}

		hsTField := reflect.TypeOf(headerStruct).Elem().Field(i)
		// fmt.Printf("RH: %v, %v, %v\n", hsTField, newFieldValue, fbuf)

		hsVField := reflect.ValueOf(headerStruct).Elem().Field(i)
		if hsVField.CanSet() {
			switch hsTField.Type.Kind() {
			case reflect.Uint16:
				hsVField.SetUint(uint64(newFieldValue.(uint16)))
			case reflect.Uint32:
				hsVField.SetUint(uint64(newFieldValue.(uint32)))
			case reflect.Uint64:
				hsVField.SetUint(newFieldValue.(uint64))
			case reflect.String:
				hsVField.SetString(newFieldValue.(string))
			case reflect.Struct:
				// TODO recursion? Maybe fancy ConvertFunc... Lets test this actually.
				return errors.New("Unmarshalling Struct, what do?")
				// hsVField.Set(hsVField)

				// TODO slices? maybe not.
			default:
				return errors.New("Hit unsupported field type, things are probably about to break.")

			}
		} else {
			return errors.New(fmt.Sprintf("Cannot set field %v of %v", i, hsTField))
		}
	}
	return nil
}

func ReadStructDescription(headerStruct interface{}, convertTable map[string]ConvertFunc) (ftds []FieldTagDescriptor) {
	ftds = make([]FieldTagDescriptor, 0)
	hft := reflect.TypeOf(headerStruct).Elem()
	// fmt.Println("RSD: hft:", hft)
	// hfv := reflect.ValueOf(headerStruct).Elem()
	// fmt.Println("RSD: hfv:", hfv)
	for i := 0; i < hft.NumField(); i++ {
		fInfo := hft.Field(i)
		// fmt.Println("RSD: fInfo:", fInfo.Name, fInfo.Type, fInfo.Tag)
		hFieldTag := ParseStructTag(fInfo.Tag, convertTable)
		ftds = append(ftds, hFieldTag)
	}
	return ftds
}

// panics if invalid
func ParseStructTag(stag reflect.StructTag, convertTable map[string]ConvertFunc) FieldTagDescriptor {
	tag := string(stag)
	ftd := FieldTagDescriptor{}
	parts := strings.Split(tag, ":")
	for i, v := range parts {
		switch i {
		case 0:
			var e Endianness
			if v == "b" {
				e = BigEnded
			} else if v == "l" {
				e = LittleEnded
			} else {
				panic("No endianness specified")
			}
			ftd.Endianness = e
		case 1:
			//TODO Handle Look up previous field to use as size?
			// e.g. "?<earlierFieldName>"
			size, err := strconv.ParseUint(v, 10, 64)
			if err != nil {
				panic("No size specified")
			}
			ftd.Size = size
		case 2:
			if v == "" {
				ftd.ConvertTo = nil
			} else if funcptr, ok := convertTable[v]; ok {
				ftd.ConvertTo = funcptr
			} else {
				panic(`ConvertTo Function for "` + v + `" doesn't exist`)
			}
		}
	}
	return ftd
}

var standardConvertTable map[string]ConvertFunc = map[string]ConvertFunc{
	"string": ConvertToString,
	"uint":   ConvertToUint,
}

// Return a copy of the default map[string]ConvertFunc, so it can be extened / overrided with user conversion functions
func CopyOfConvertTable() map[string]ConvertFunc {
	convertTable := make(map[string]ConvertFunc)
	for k, v := range standardConvertTable {
		convertTable[k] = v
	}
	return convertTable
}

func ConvertToString(b []byte, h FieldTagDescriptor) (interface{}, error) {
	return string(b), nil
}

func ConvertToUint(b []byte, ftd FieldTagDescriptor) (interface{}, error) {
	switch ftd.Size {
	case 1:
		return b[0], nil
	case 2:
		if ftd.Endianness == BigEnded {
			return binary.BigEndian.Uint16(b), nil
		} else {
			return binary.LittleEndian.Uint16(b), nil
		}
	case 4:
		if ftd.Endianness == BigEnded {
			return binary.BigEndian.Uint32(b), nil
		} else {
			return binary.LittleEndian.Uint32(b), nil
		}
	case 8:
		if ftd.Endianness == BigEnded {
			return binary.BigEndian.Uint64(b), nil
		} else {
			return binary.LittleEndian.Uint64(b), nil
		}
	default:
		return nil, errors.New("Unsupported field size")
	}
}
